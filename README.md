# `nefertari-sqla`
[![Build Status](https://travis-ci.org/brandicted/nefertari-sqla.svg?branch=master)](https://travis-ci.org/brandicted/nefertari-sqla)
[![Documentation](https://readthedocs.org/projects/nefertari-sqla/badge/?version=stable)](http://nefertari-sqla.readthedocs.org)

SQLA backend for Nefertari
